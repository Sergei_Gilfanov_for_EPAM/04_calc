package calc;

public class AdditionTailItem extends ExprTailItem {

  AdditionTailItem(Factor aFactor) {
    super(aFactor);
  }

  public CalcValue applyTo(CalcValue head) {
    return head.add(this.factor.getValue());
  }
}
