package calc;

public class DivisionTailItem extends FactorTailItem {

  DivisionTailItem(Term aTerm) {
    super(aTerm);
  }

  public CalcValue applyTo(CalcValue head) {
    return head.divide(this.term.getValue());
  }
}
