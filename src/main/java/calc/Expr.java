package calc;

import java.util.List;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Абстракция для представления выражения из операторов сложения и вычитания.
 */
public class Expr {
  Factor head;
  List<ExprTailItem> tail;

  private Expr(Factor aHead, List<ExprTailItem> aTail) {
    head = aHead;
    tail = aTail;
  }

  public CalcValue getValue() {
    CalcValue value;
    if (head == null) {
      value = new CalcValue(0);
    } else {
      value = head.getValue();
    }
    for (ExprTailItem tailItem : tail) {
      value = tailItem.applyTo(value);
    }
    return value;
  }

  static Expr parse(CalcReader reader)
      throws IOException, ParseErrorException, TokenNotFoundException {
    // Это может вызвать, TokenNotFoundException, что означает, что последовательности cлагаемых у
    // нас нет,
    Factor head;
    try {
      head = Factor.parse(reader);
    } catch (TokenNotFoundException exp) {
      head = null;
    }

    List<ExprTailItem> tail = new ArrayList<ExprTailItem>();

    boolean done = false;
    do {
      try {
        ExprTailItem tailItem = ExprTailItem.parse(reader);
        tail.add(tailItem);
      } catch (TokenNotFoundException exp) {
        done = true;
      }
    } while (!done);

    // Тут пользуемся знанием о всей грамматике - выражение может оканчиваться только на скобку или
    // конец ввода
    int c;
    while (Character.isWhitespace(c = reader.read()));
    if (c != ')' && c != -1) {
      // нужно для правильного вывода ошибки, иначе в контексте будет выведен и
      // последний (неправильный) символ
      reader.unread(c);
      throw new ParseErrorException("Ожидается слагаемое");
    }

    if (head == null && tail.size() == 0) {
      reader.unread(c);
      throw new TokenNotFoundException();
    }
    reader.unread(c);
    return new Expr(head, tail);
  }

}
