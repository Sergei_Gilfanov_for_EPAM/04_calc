package calc;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class ParenthesesTermTest {

	@Test
	public void testParseParenthesesTerm() throws IOException, ParseErrorException,TokenNotFoundException{
		String exprAsString = "(1)";
	    CalcReader reader = new CalcReader(exprAsString);
	    Term term = ParenthesesTerm.parseParenthesesTerm(reader);
	    CalcValue value = term.getValue();
	    assertTrue("One term", value.equals(1));
	}

	@Test(expected=ParseErrorException.class)
	public void testParseEmpty() throws IOException, ParseErrorException,TokenNotFoundException{
		String exprAsString = "()";
	    CalcReader reader = new CalcReader(exprAsString);
	    Term term = ParenthesesTerm.parseParenthesesTerm(reader); // execution will not pass this line
	    CalcValue value = term.getValue();
	    fail(String.format("Parse error not catched, got %f", value));
	}
}
