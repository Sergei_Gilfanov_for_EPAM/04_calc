package calc;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class FactorTailItemTest {

  @Test
  public void testParseFactorTailItemMultType() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "* -12e5";
    CalcReader reader = new CalcReader(valueAsString);
    FactorTailItem item = FactorTailItem.parse(reader);
    assertTrue("'*...' should give MultiplicationTailItem", item instanceof MultiplicationTailItem);
  }

  @Test
  public void testParseFactorTailItemMultApply() throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(2);
    
    String valueAsString = "* -3";
    CalcReader reader = new CalcReader(valueAsString);
    FactorTailItem item = FactorTailItem.parse(reader);
    head = item.applyTo(head);
    assertTrue("MultiplicationTailItem: wrong calculations", head.equals(-6));
  }

  
  @Test
  public void testParseFactorTailItemDivType() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "/ -12e5";
    CalcReader reader = new CalcReader(valueAsString);
    FactorTailItem item = FactorTailItem.parse(reader);
    assertTrue("'/....' should give DivisionTailItem", item instanceof DivisionTailItem);
  }
  
  @Test
  public void testParseFactorTailItemDivApply() throws IOException, ParseErrorException, TokenNotFoundException {
    CalcValue head = new CalcValue(-6);
    
    String valueAsString = "/ -3";
    CalcReader reader = new CalcReader(valueAsString);
    FactorTailItem item = FactorTailItem.parse(reader);
    head = item.applyTo(head);
    assertTrue("DivisionTailItem: wrong calculations", head.equals(2));
  }
  
  @Test(expected=ParseErrorException.class)
  public void testParseFactorTailItemNoTerm() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = "/ q";
    CalcReader reader = new CalcReader(valueAsString);
    FactorTailItem.parse(reader); // execution will not pass this line
  }
  
  @Test(expected=TokenNotFoundException.class)
  public void testParseFactorTailItemNoFactorTail() throws IOException, ParseErrorException, TokenNotFoundException {
    String valueAsString = " ";
    CalcReader reader = new CalcReader(valueAsString);
    FactorTailItem.parse(reader); // execution will not pass this line
  }
}
